#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Imports


# In[1]:


import sqlite3
import bcrypt
import flet
from flet import (
    Checkbox,
    Column,
    FloatingActionButton,
    IconButton,
    OutlinedButton,
    Page,
    page,
    Row,
    Tab,
    Tabs,
    Text,
    TextField,
    UserControl,
    colors,
    icons,
    NavigationRail,
    NavigationRailDestination,
    VerticalDivider,
    Icon,
    icons,
    Container,
    alignment,
    PopupMenuButton,
    PopupMenuItem,
    AppBar,
    FilledButton,
    Dropdown,
    dropdown,
    Tab,
    Tabs,
    Card,
    ListTile,
    padding,
    View,
    view,
    ElevatedButton,
    AlertDialog,
    padding,
    Banner,
    Card,
    Switch,
    AnimatedSwitcher,
    theme,
    Theme,
    border_radius,
)

# In[ ]:


# In[ ]:


# Database


# In[ ]:


connection = sqlite3.connect("Tracker_buddy_v0.3.db")  # creates the database

# In[ ]:


cursor = connection.cursor()  # creates connection

# In[ ]:


# create a table
cursor.execute(
    """
    CREATE TABLE users (
    username text,
    password text
    )
    """
)

# In[ ]:


# this wouldn't have encryption
# cursor.execute(
#     "INSERT INTO users VALUES (:username, :password)", 
#     {'username': "erik", 'password': "123"}
# )


# In[ ]:


cursor.execute("SELECT * FROM users")
search = cursor.fetchall()

# In[ ]:


search

# In[ ]:


connection.commit()

# In[ ]:


connection.close()


# In[ ]:


# In[ ]:


# classes


# In[2]:


class Assignment(UserControl):

    def __init__(self, assignment_name):
        super().__init__()
        self.assignment_name = assignment_name

    def build(self):
        self.new_step = TextField(
            hint_text="Add a step",
            border_color="#622678",
            expand=True
        )

        self.display_assignment = Checkbox(
            value=False,
            label="",
        )

        self.item = Column()
        self.card = Card(
            content=Container(
                content=Column(
                    [
                        Text(self.assignment_name),
                        self.item,
                        Row(
                            controls=[
                                self.new_step,
                                FloatingActionButton(
                                    icon=icons.ADD,
                                    bgcolor="#622678",
                                    on_click=self.add_step
                                ),
                            ]
                        ),
                    ],
                ),
                width=600,
                padding=50,
                bgcolor="#26284a",
                border_radius=border_radius.all(15),
            )
        )

        return self.card

    def add_step(self, e):
        self.item.controls.append(Checkbox(label=self.new_step.value))
        self.new_step.value = ""
        self.update()


# In[3]:


class TrackerBuddy(UserControl):

    def build(self):
        self.assignment_title = TextField(
            hint_text="Assignment Title",
            border_color="#622678",
            expand=True
        )

        self.board = Column()

        self.wall = Column(
            width=600,
            alignment="center",
            controls=[
                Row(
                    controls=[
                        self.assignment_title,
                        FloatingActionButton(
                            icon=icons.ADD,
                            bgcolor="#622678",
                            on_click=self.add_clicked),
                    ],
                ),
                self.board,
            ],
        )

        return self.wall

    def add_clicked(self, e):
        if self.assignment_title.value == "":
            return

        assignment = Assignment(self.assignment_title.value)
        self.board.controls.append(assignment)
        self.assignment_title.value = ""
        self.update()


# In[4]:


# this bit of code I use to check individual things quickly

def main(page: Page):
    page.title = "Tracker Buddy"
    page.horizontal_alignment = "center"
    page.vertical_alignment = "center"
    page.bgcolor = "#14152b"
    page.window_width = 1000
    page.window_height = 600
    page.scroll = "auto"

    tb = TrackerBuddy()

    page.add(tb)


# flet.app(target=main) # uncomment to run this code


# In[5]:


class User(UserControl):

    def __init__(self, username, password):
        super().__init__()
        self.username = username
        self.password = password


# In[6]:


class SignUp(UserControl):

    def build(self):
        self.enter_username = TextField(
            hint_text="Username",
            label="Username",
            border_color="#622678",
            expand=True
        )

        self.enter_password = TextField(
            hint_text="Password",
            label="Password",
            border_color="#622678",
            password=True,
            can_reveal_password=True,
            expand=True
        )

        self.signup_page = Column(
            width=600,
            horizontal_alignment="center",
            controls=[
                Text("Sign Up Page", size=40),
                Row(
                    controls=[
                        self.enter_username,
                    ],
                ),
                Row(
                    controls=[
                        self.enter_password,
                    ],
                ),
            ],

        )

        return Column(
            controls=[
                self.signup_page
            ]
        )

    def signup_button(self, e):

        connection = sqlite3.connect("Tracker_buddy_v0.3.db")
        cursor = connection.cursor()

        username = self.enter_username.value
        password = self.enter_password.value

        salt = bcrypt.gensalt()

        hashed_password = bcrypt.hashpw(password.encode('utf8'), salt)
        hashed_password = hashed_password.decode('utf8')

        cursor.execute(f"SELECT * FROM users WHERE username ='{username}'")
        search = cursor.fetchone()

        if search == None:
            search = ("", "")

        if username == search[0]:
            print("Username already exists. Try a different one.")
            return False

        else:
            cursor.execute(
                "INSERT INTO users VALUES (:username, :password)",
                {'username': f"{username}", 'password': f"{hashed_password}"}
            )

            connection.commit()
            connection.close()

            print("User Created")
            return True

        self.update()


# In[12]:


class Login(UserControl):

    def build(self):
        self.enter_username = TextField(
            hint_text="Username",
            label="Username",
            border_color="#622678",
            expand=True
        )

        self.enter_password = TextField(
            hint_text="Password",
            label="Password",
            border_color="#622678",
            password=True,
            can_reveal_password=True,
            expand=True
        )

        self.login_page = Column(
            width=600,
            horizontal_alignment="center",
            controls=[
                Text("Login Page", size=40),
                Row(
                    controls=[
                        self.enter_username,
                    ],
                ),
                Row(
                    controls=[
                        self.enter_password,
                    ],
                ),
            ],

        )

        return Column(
            controls=[
                self.login_page
            ]
        )

    def login_button(self, e):

        connection = sqlite3.connect("Tracker_buddy_v0.3.db")
        cursor = connection.cursor()

        username = self.enter_username.value
        password = self.enter_password.value

        password = password.encode('utf8')

        cursor.execute(f"SELECT * FROM users WHERE username ='{username}'")
        search = cursor.fetchone()

        if search == None:
            return False

        hashed = str.encode(search[1])

        if username == search[0] and bcrypt.checkpw(password, hashed):
            print("Match")
            return True
        else:
            print("No Match")
            return False

        connection.commit()
        connection.close()
        self.update()


# In[13]:


# page


# In[14]:


def main(page: Page):
    # page theme & settings

    page.title = "Tracker Buddy"
    page.horizontal_alignment = "center"
    page.vertical_alignment = "center"

    bg = page.bgcolor = "#14152b"
    #     page.bgcolor="#14152b"
    #     page.platform = "window"
    page.overlay

    page.window_width = 1000
    page.window_height = 600
    page.scroll = "auto"
    page.update()

    # ---------------------

    # theme switch 
    def switch_mode(e):

        if switch.label == "Light Mode":
            page.bgcolor = "#14152b"
            switch.label = "Dark Mode"
            page.update()

        else:
            page.bgcolor = "#7478c2"
            switch.label = "Light Mode"
            page.update()

    switch = Switch(
        label="Dark Mode",
        label_position="left",
        on_change=switch_mode
    )
    # ---------------------

    # appbar theme
    appbar = page.appbar = AppBar(
        leading=Icon(icons.ASSIGNMENT),
        leading_width=40,
        title=Text("Tracker Buddy"),
        center_title=True,
        bgcolor="#26284a",
        actions=[switch]
    )
    # ---------------------

    # classes/pages
    login = Login()
    signup = SignUp()
    tracker = TrackerBuddy()

    # ---------------------

    # banner
    def open_banner(e):
        page.banner.open = True
        page.update()

    def close_banner(e):
        page.banner.open = False
        page.update()

    banner = page.banner = Banner(
        bgcolor="#450a1f",
        leading=Icon(
            icons.WARNING_AMBER_ROUNDED,
            color=colors.WHITE,
            size=40
        ),
        content=Text(
            "User not found. Please, try again.",
            size=20
        ),
        actions=[
            ElevatedButton(
                text="Retry",
                color=colors.WHITE,
                bgcolor="#2e0313",
                on_click=close_banner
            ),
        ],
    )

    # ---------------------

    # button actions
    def log_in(e):
        verify = login.login_button(e)

        if verify:
            page.go("/home")
        else:
            open_banner(e)

    def sign_up(e):
        verify = signup.signup_button(e)

        if verify:
            page.go("/login")
        else:
            open_banner(e)

    # ---------------------

    # buttons
    log_in_button = ElevatedButton(
        text="Log In",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=log_in,
    )

    make_account_button = ElevatedButton(
        text="Make an Account",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=lambda _: page.go("/signup")
    )

    sign_up_button = ElevatedButton(
        text="Sign Up",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=sign_up,
    )

    back_to_login_button = ElevatedButton(
        text="I Have an Account",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=lambda _: page.go("/login")
    )

    login_buttons_list = [log_in_button, make_account_button]
    sign_up_buttons_list = [sign_up_button, back_to_login_button]

    # ---------------------

    # navigation across app
    def route_change(route):
        page.views.clear()
        page.views.append(
            View(
                "/login",
                [
                    appbar,
                    login,
                    Row(
                        alignment="center",
                        controls=login_buttons_list,
                    ),
                ],
                horizontal_alignment="center",
            )
        )

        if page.route == "/signup":
            page.views.append(
                View(
                    "/signup",
                    [
                        appbar,
                        signup,
                        Row(
                            alignment="center",
                            controls=sign_up_buttons_list,
                        ),
                    ],
                    horizontal_alignment="center",

                )
            )

        elif page.route == "/home":
            page.views.append(
                View(
                    "/home",
                    [
                        appbar,
                        tracker,
                    ],
                    horizontal_alignment="center",
                )
            )

        page.update()

    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    page.on_route_change = route_change
    page.on_view_pop = view_pop
    page.go(page.route)
    page.update()
    # ---------------------

    page.add()


flet.app(target=main)  # app
# flet.app(target=main, view=flet.WEB_BROWSER) # browser


# In[ ]:


# In[113]:


# testing random things \/


# In[149]:


import flet
from flet import Container, Icon, Page, Tab, Tabs, Text, alignment, icons, theme


def main(page: Page):
    # page theme & settings
    page.title = "Tracker Buddy"
    page.horizontal_alignment = "center"
    page.vertical_alignment = "center"
    page.bgcolor = "#14152b"
    page.window_width = 1000
    page.window_height = 600
    page.scroll = "always"
    # ---------------------

    sun = Icon(icons.LIGHT_MODE_OUTLINED)
    moon = Icon(icons.MODE_NIGHT_OUTLINED)

    def switch_change(e):

        if switch.label == "Light Mode":
            page.bgcolor = "#14152b"
            #             switch.label="Dark Mode"
            appbar.actions[0] = moon
            page.update()

        else:
            page.bgcolor = "#7478c2"
            #             switch.label="Light Mode"
            appbar.actions[0] = sun
            page.update()

    switch = Switch(label="Dark Mode", label_position="left", on_change=switch_change)  #

    # appbar theme

    icon = Icon()

    appbar = page.appbar = AppBar(
        leading=Icon(icons.ASSIGNMENT),
        leading_width=40,
        title=Text("Tracker Buddy"),
        center_title=True,
        bgcolor="#26284a",
        actions=[icon, switch]
    )
    # ---------------------

    print(appbar.actions[0])
    page.add()


flet.app(target=main)  # uncomment to run this code

# In[ ]:
