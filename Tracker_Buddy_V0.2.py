#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Imports


# In[43]:


import sqlite3
import flet
from flet import (
    Checkbox,
    Column,
    FloatingActionButton,
    IconButton,
    OutlinedButton,
    Page,
    Row,
    Tab,
    Tabs,
    Text,
    TextField,
    UserControl,
    colors,
    icons,
    NavigationRail,
    NavigationRailDestination,
    VerticalDivider,
    Icon,
    icons,
    Container,
    alignment,
    PopupMenuButton,
    PopupMenuItem,
    AppBar,
    FilledButton,
    Dropdown,
    dropdown,
    Tab,
    Tabs,
    Card,
    ListTile,
    padding,
    View,
    view,
    ElevatedButton,
    AlertDialog,
    padding,
    Banner,
    Card,
)


# In[ ]:





# In[44]:


# Database


# In[45]:


connection = sqlite3.connect("Tracker_buddy_v0.1.db") # creates the database


# In[46]:


cursor = connection.cursor() # creates connection


# In[47]:


# create a table
# cursor.execute(
#     """
#     CREATE TABLE users (
#     username text,
#     password text
#     )
#     """
# ) 


# In[48]:


# cursor.execute(
#     "INSERT INTO users VALUES (:username, :password)", 
#     {'username': "erik", 'password': "123"}
# )


# In[49]:


cursor.execute("SELECT * FROM users")
search = cursor.fetchall()


# In[50]:


search[0][1]


# In[51]:


connection.commit()


# In[ ]:


connection.close()


# In[ ]:





# In[ ]:


# classes


# In[ ]:


# old and unused \/


# In[ ]:


class Task(UserControl):
    
    def __init__(self, task_name, task_status_change, task_delete):
        super().__init__()
        self.completed = False
        self.task_name = task_name
        self.task_status_change = task_status_change
        self.task_delete = task_delete
    
    def build(self):
        self.new_task = TextField(hint_text="Whats needs to be done?", expand=True)
        self.tasks = Column()

        # application's root control (i.e. "view") containing all other controls
        return Column(
            width=600,
            alignment="center",
            controls=[
                Row(
                    controls=[
                        self.new_task,
                        FloatingActionButton(icon=icons.ADD, on_click=self.add_clicked),
                    ],
                ),
                self.tasks,
            ],
        )

    def add_clicked(self, e):
        self.tasks.controls.append(Checkbox(label=self.new_task.value))
        self.new_task.value = ""
        self.update()


# In[ ]:


class Course(UserControl):
    
    def build(self):
        self.new_course = FilledButton(text="COSC 3337")
        
        return Column(
            width=600,
            alignment="center",
            controls=[
                Row(
                    controls=[
                        self.new_course,
                    ],
                ),

            ],
        )


# In[ ]:


# old and unused /\


# In[ ]:





# In[ ]:


# currently using \/


# In[232]:


class TrackerBuddy(UserControl):
    
    def build(self):
        self.assignment_title = TextField(hint_text="Assignment Title", expand=True)
        self.new_step = TextField(hint_text="Add a step", expand=True)
        
#         self.new_course = TextField(hint_text="Course Name and Number", expand=True)
#         self.courses = Tabs(
#             tabs=[],
#         )

        self.item = Column()
        self.board = Column()
        self.wall = Column(
            width=600,
            alignment="center",
            controls=[
                Row(
                    controls=[
                        self.assignment_title,
                        FloatingActionButton(icon=icons.ADD,
                                             on_click=self.add_clicked),
                    ],
                ),
                self.board,
#                 self.item, 
            ],
        )

        return self.wall

    def add_clicked(self, e):
#         page.views.clear()
        
        if self.assignment_title.value == "":
            return
        
#         self.courses.tabs.append(Tab(text=self.new_course.value))
#         self.courses.tab_content.append(Checkbox(label=self.new_course.value))

        self.board.controls.append(Card(
            content=Container(
                content=Column(
                    [  
                        Text(self.assignment_title.value),
                        self.item,
                        Row(
                            controls=[
                                self.new_step,
                                FloatingActionButton(
                                    icon=icons.ADD,
                                    on_click=self.add_step
                                ),
                            ]
                        )
                        
                    ]
                ),
                width=600,
                padding=50,
            )
        ))
        
#         self.board.controls.append(Checkbox(label=self.new_step.value))
    
        self.assignment_title.value = ""
        self.update()
        
    def add_step(self, e):
        print(self.board.controls[0].content.content.controls[1].controls)
#         self.board.controls[0].content.content.controls[1].controls.append(self.item)
#         print(self.board.controls[0].content.content.controls[1].controls[2])
#         self.board.controls[0].content.content.controls[1].controls[2].controls.append(Checkbox(label=self.new_step.value))


# In[233]:


def main(page: Page):
    page.title="Tracker Buddy"
    page.horizontal_alignment="center"
    page.vertical_alignment="center"
    page.bgcolor="#14152b" 
    page.window_width=1000
    page.window_height=600
    page.scroll="auto"
    
    tb = TrackerBuddy()
    
    card = Card(
        content=Container(
            content=Column(
                [  
                    Text("Assignment 3"),
                    tb,
                ]
            ),
            width=400,
            padding=10,
        )
    )
    
    card2 = Card(
        content=Container(
            Text("Hello Flet"),
            width=400,
            padding=10,
        )
    )
    
    page.add(tb)

flet.app(target=main)


# In[ ]:


### This main is for testing the assignment adding /\


# In[53]:


class User(UserControl):
    
    def __init__(self, username, password):
        super().__init__()
        self.username = username
        self.password = password


# In[54]:


class Login(UserControl):
    
    def build(self):
        self.enter_username = TextField(
            hint_text="Username", 
            label="Username",
            border_color="#622678",
            expand=True
        )
        
        self.enter_password = TextField(
            hint_text="Password", 
            label="Password",
            border_color="#622678",
            password=True, 
            can_reveal_password=True,
            expand=True
        )
        
        self.login_page = Column(
            width=600,
            horizontal_alignment="center",
            controls=[
                Text("Login Page", size=40),
                Row(
                    controls=[
                        self.enter_username,
                    ],
                ),
                Row(
                    controls=[
                        self.enter_password,
                    ],
                ),
#                 Row(
#                     alignment="center",
#                     controls=[
#                         ElevatedButton(
#                             text="Log in",
#                             color=colors.WHITE,
#                             bgcolor="#622678",
#                             on_click=self.login_button
#                         ),
#                     ],
#                 ),
            ],
        
        )

        # application's root control (i.e. "view") containing all other controls
        return Column(
            controls=[
                self.login_page
            ]
        )

    def login_button(self, e):
        profile = User(self.enter_username.value,self.enter_password.value)
        
        if profile.username == search[0][0] and profile.password == search[0][1]:
#             self.login_page.controls.append(Text("User in database"))
            return True
        else:
#             self.login_page.controls.append(Text("User NOT in database"))
            return False
        
        self.update()
        
        


# In[55]:


class SignUp(UserControl):
    
    def build(self):
        self.enter_username = TextField(
            hint_text="Username", 
            label="Username",
            border_color="#622678",
            expand=True
        )
        
        self.enter_password = TextField(
            hint_text="Password", 
            label="Password",
            border_color="#622678",
            password=True, 
            can_reveal_password=True,
            expand=True
        )
        
        self.login_page = Column(
            width=600,
            horizontal_alignment="center",
            controls=[
                Text("Sign Up Page", size=40),
                Row(
                    controls=[
                        self.enter_username,
                    ],
                ),
                Row(
                    controls=[
                        self.enter_password,
                    ],
                ),
                Row(
                    alignment="center",
                    controls=[
                        ElevatedButton(
                            text="Sign Up",
                            color=colors.WHITE,
                            bgcolor="#622678",
                            on_click=self.signup_button
                        ),
                    ],
                ),
            ],
        
        )

        # application's root control (i.e. "view") containing all other controls
        return Column(
            controls=[
                self.login_page
            ]
        )

    def signup_button(self, e):
        print("Yep")
        
        self.update()


# In[ ]:


# currently using /\


# In[ ]:





# In[ ]:


# page


# In[162]:


def main(page: Page):
    
    # page theme & settings 
    page.title="Tracker Buddy"
    page.horizontal_alignment="center"
    page.vertical_alignment="center"
    page.bgcolor="#14152b" 
    page.window_width=1000
    page.window_height=600
    page.scroll="auto"
    # ---------------------
    
    
    # appbar theme
    appbar = page.appbar = AppBar(
        leading=Icon(icons.ASSIGNMENT),
        leading_width=40,
        title=Text("Tracker Buddy"),
        center_title=True,
        bgcolor="#26284a",
    )
    # ---------------------
    
    
    # classes/pages
    login = Login()
    signup = SignUp()
    tracker = TrackerBuddy()
    # ---------------------
    
    
    # banner
    def open_banner(e):
        page.banner.open = True
        page.update()
        
    def close_banner(e):
        page.banner.open = False
        page.update()
        
    banner = page.banner = Banner(
        bgcolor="#450a1f",
        leading=Icon(
            icons.WARNING_AMBER_ROUNDED, 
            color=colors.WHITE, 
            size=40
        ),
        content=Text(
            "User not found. Please, try again.", 
            size=20
        ),
        actions=[
            ElevatedButton(
                text="Retry", 
                color=colors.WHITE, 
                bgcolor="#2e0313",
                on_click=close_banner
            ),
        ],
    )
    # ---------------------
    
    
    # button actions
    def log_in(e):
        verify = login.login_button(e)
        
        if verify:
            page.go("/home")
        else:
            open_banner(e)
            
    # ---------------------
    
    
    # buttons
    log_in_button = ElevatedButton(
        text="Log In",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=log_in,
    )
    
    sign_up_button = ElevatedButton(
        text="Make an Account",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=lambda _: page.go("/signup"), 
    )
    
    back_to_login_button = ElevatedButton(
        text="I Have an Account",
        color=colors.WHITE,
        bgcolor="#622678",
        on_click=lambda _: page.go("/login")
    ),
    
    login_buttons_list = [log_in_button, sign_up_button]
    # ---------------------
    
    
    # navigation across app
    def route_change(route):
        page.views.clear()
        page.views.append(
            View(
                "/login",
                [ 
                    appbar,
                    login,
                    Row(
                        alignment="center",
                        controls=login_buttons_list,
                    ),
                ],
                horizontal_alignment="center",
            )
        )
        if page.route == "/signup":
            page.views.append(
                View(
                    "/signup",
                    [
                        appbar,
                        signup,
                        Row(
                            alignment="center",
                            controls=back_to_login_button,
                        ),
                    ],
                    horizontal_alignment="center",
                )
            )
        
        elif page.route == "/home":
            page.views.append(
                View(
                    "/home",
                    [
                        appbar,
                        tracker,
                    ],
                    horizontal_alignment="center",
                )
            )
        
        page.update()

    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    page.on_route_change = route_change
    page.on_view_pop = view_pop
    page.go(page.route)
    # ---------------------
    
    
flet.app(target=main)


# In[ ]:





# In[ ]:


# testing random things \/


# In[35]:


import flet
from flet import Container, Icon, Page, Tab, Tabs, Text, alignment, icons

def main(page: Page):
    track = TrackerBuddy()
    

    t = Tabs(
        selected_index=0,
        animation_duration=300,
        tabs=[
            Tab(
                text="Tab 1",
                content=Container(
                    track,
                ),
            ),
            Tab(
                tab_content=Icon(icons.SEARCH),
                content=Text("This is Tab 2"),
            ),
        ],
        expand=1,
    )

    page.add(t)

flet.app(target=main)


# In[42]:


# working with Row #
def main(page: Page):
    page.horizontal_alignment="center"
    page.vertical_alignment="center"
    
    text_1 = Text("Hello Flet")
    text_2 = Text("FLET")
    
    txt_lst = [text_1, text_2]
    
    row = Row(alignment="center", controls=txt_lst)
    
    box = Container(
        content=text_1,
        bgcolor=colors.DEEP_PURPLE_900,
        padding=50,
        ink=True,
        on_click=lambda e: print("Clickable with Ink clicked!"),
    )
    

    page.add(box)

flet.app(target=main)


# In[40]:


import flet
from flet import Card, Column, Container, Icon, ListTile, Row, Text, TextButton, icons

def main(page: Page):
    page.title = "Card Example"
    page.horizontal_alignment="center"
    page.vertical_alignment="center"
    
    tb = TrackerBuddy()
    
    card = Card(
        content=Container(
            content=Column(
                [  
                    Text("Assignment 3"),
                    tb,
                ]
            ),
            width=400,
            padding=10,
        )
    )
    
    card2 = Card(
        content=Container(
            Text("Hello Flet"),
            width=400,
            padding=10,
        )
    )
    
    page.add(card)

flet.app(target=main)


# In[ ]:




